import {CHANGE_MESSAGE, SUCCESS_DECODED, SUCCESS_ENCODED} from "./actionTypes";

const initialState = {
  toEncodeMessage: '',
  toDecodeMessage: '',
  password: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_MESSAGE:
      return {...state, [action.targetName]: action.targetValue};
    case SUCCESS_ENCODED:
      return {...state,
        toEncodeMessage: action.encodedMessage,
        toDecodeMessage: ''};
    case SUCCESS_DECODED:
      return {...state,
        toDecodeMessage: action.decodedMessage,
        toEncodeMessage: ''
      };
    default:
      return state;
  }
};

export default reducer;