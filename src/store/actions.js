import {CHANGE_MESSAGE, SUCCESS_DECODED, SUCCESS_ENCODED} from "./actionTypes";
import axios from "../axios-api";


export const changeMessagesHandler = (targetName, targetValue) => {
  return {type: CHANGE_MESSAGE, targetName, targetValue};
};

export const cipheringMessage = (type, message) => dispatch => {
  if (type === 'encode') {
    axios.post('/encode', {message})
      .then(resp => {
        resp.data.encoded.length
          ? dispatch(successEncoded(resp.data.encoded))
          : alert('Enter correct data!');
      });
  } else {
    axios.post('/decode', {message})
      .then(resp => {
        resp.data.decoded.length
          ? dispatch(successDecoded(resp.data.decoded))
          : alert('Enter correct data!');
      });
  }
};

export const successEncoded = encodedMessage => {
  return {type: SUCCESS_ENCODED, encodedMessage};
};

export const successDecoded = decodedMessage => {
  return {type: SUCCESS_DECODED, decodedMessage};
};