import React, { Component } from 'react';
import {connect} from "react-redux";

import './App.css';
import {changeMessagesHandler, cipheringMessage} from "./store/actions";


class App extends Component {

  onChangeMessagesHandler = event => {
    this.props.changeMessagesHandler(event.target.name, event.target.value);
  };

  cipheringHandler = (type, encodedMessage) => {
    let message = {
      message: encodedMessage ? encodedMessage : this.props.toDecodeMessage,
      password: this.props.password,
    };

    if (this.props.password.length) {
      this.props.cipheringMessage(type, message);
    }
  };

  render() {
    return (
      <div className="App">
          <div>
            <label>Decoded message</label>
            <textarea
              value={this.props.toDecodeMessage}
                      onChange={this.onChangeMessagesHandler}
                      name='toDecodeMessage'
                      cols="30"
                      rows="5"/>
          </div>
          <div>
            <label>Password</label>
            <input type="password"
                   name='password'
                   required
                   value={this.props.password}
                   onChange={this.onChangeMessagesHandler}/>
            <button onClick={() => this.cipheringHandler('decode', this.props.toEncodeMessage)}>Decode</button>
            <button onClick={() => this.cipheringHandler('encode')}>Encode</button>
          </div>
          <div>
            <label>Encoded message</label>
            <textarea value={this.props.toEncodeMessage}
                      onChange={this.onChangeMessagesHandler}
                      name='toEncodeMessage'
                      cols="30"
                      rows="5"/>
          </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    toEncodeMessage: state.toEncodeMessage,
    toDecodeMessage: state.toDecodeMessage,
    password: state.password,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    changeMessagesHandler: (targetName, targetValue) => dispatch(changeMessagesHandler(targetName, targetValue)),
    cipheringMessage: (type, message) => dispatch(cipheringMessage(type, message)),
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(App);
